+++
title = "Work-history and skills"
date = "2020-01-29"
+++

#### [Hyphen - Be Heard at Work](https://gethyphen.com/)
*Software Engineer (full stack)* -  (Sep 2018 to current)

Hyphen helps organizations better understand their employees. I'm part of the engineering team for building **Insights**, an app for launching instant surveys & pulse-polls to gather continuous feedback & drive engagement at any stage of the employee lifecycle.

- I played a role in several key development related activities including, but not limited to, front-end architecture, designing RESTful web services, documenting code and defining acceptance criteria, writing unit tests, and resolving issues for existing customers.

- As part of work, I deal with these technologies: Javascript + TypeScript, React+Redux, Jest, Ant-Design, NodeJS & Express, MongoDB.

---

#### [Udacity](https://www.udacity.com/)
*Classroom mentor and project-reviewer* - (Nov 2017 to Jul 2018)

This was an independent contractor role, so I wasn't directly an employee of Udacity. I was a mentor at two of Udacity's programs - Machine Learning Engineer Nanodegree and Deep Learning Engineer Nanodegree. The reason being - I wanted to get a better understanding of these topics myself! They say that the best way to learn is to teach right?

My work involved:

- Motivating students when they find the course too difficult.
- Helping them understand certain concepts better, and providing them with additional materials for improving their understanding of the overall content
- Reviewing code and offering feedback

---

#### [Adori Labs](https://www.adorilabs.com/)
*Software Engineer (Full Stack)* - (Dec 2015 - Aug 2016)

Adori Labs offers a platform for people who publish audio content, provide them with detailed analytics, and to help them further drive engagement.

I was a very early employee of this company, so along with the team we did the following:

- Building the web service for the mobile phone podcast app. The existing code was written using Go, so I got to learn and use Go as well.
- Building a Web scraper that would crawl through podcast sources and store them.
- A web portal to allow content publishers upload audio content, with a minimal set of tools to edit them.

---

#### [Toorq Media Services](https://angel.co/company/toorq-media-services-1)
*Engineering Lead and full-stack developer* - (Nov 2014 - Sep 2015)

Toorq (pronounced *Torque*) was a hyper-local social networking app with built-in privacy features. You could for-example, type in a place of your choice, and the app would display contextual information and posts from that place. You could create posts for the world to see, upvote posts from other people, comment on them. Sorta like Facebook's better, not-evil counterpart! 😉

I joined the organization as a front-end developer initially, eventually taking lead of several of the engineering efforts:

- Building the web front-end for the platform and RESTful web services.
- Hiring developers - when I joined, there were just two engineers - we managed to grow to a size of 10
- Worked with our very talented in-house designer to build features
- Worked with interns from nearby colleges and trained them in technologies like JavaScript, React, NodeJS and MongoDB.

The company is sadly now defunct, as the founders were unable to raise seed funding. But I had some of the most fun-filled times there.

---

# Skills

### Programming Skills

I'm skilled with several front-end technologies, including but not limited to:

* HTML5, JavaScript/TypeScript, ReactJS-Redux ecosystem & their related tools. I also have a decent exposure to libraries like styled-components, Ant-Design, Jest/Enzyme, Webpack, testing tools like Jest & Enzyme.

* I tend to think in-terms of what constitutes for good user-experience and web performance.

I also like working on the back-end side of things and have used the following tools at work:

* NodeJS, Express and NPM.   
* MongoDB as the data-store.
* I've also got some exposure to Python and libraries like scikit-learn and keras.


### Other skills

* I can communicate very well with writing.
* I can [edit and optimize content](/basecamp-application/posts/my-portfolio/#i-like-to-help-other-people-write-as-well) as well.
* I also like creating nice cover images and often do so for my blogs, like [this one](https://blog.booleanhunter.com/using-machine-learning-to-predict-the-quality-of-wines/).
* I can tell you a thing or two about diet, fitness and meditation.  🍱💪😇