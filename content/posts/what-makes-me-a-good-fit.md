+++
title = "Why me?"
date = "2020-01-29"
+++

**I'm experimental.**

And I especially enjoy work that involves research and toying with ideas to build something useful, and making them open-source. The creative process of building something great isn't necessarily pleasant. Many times you'd be blocked with complexities, and the tools at your disposal won't be sufficient. You'd have to eventually figure a way out of these challenges and make progress. I love and welcome challenges!

Respecting the creative process is what helped me get involved in [writing](https://medium.com/transient-sunshine-of-the-spotted-mind/the-journey-towards-creative-writing-2870a09adf6b) and [coding](https://medium.com/hackernoon/how-to-know-if-programming-is-your-cup-of-tea-94c4b1391a)  - so I understand and appreciate its power. I think that someone who not only realizes this, but experiences it almost every-day can do amazingly well in this position, being part of the Research & Fidelity team. I'm that person.

I think that when you put necessity and enthusiasm together, one can build great works like projects such as Stimulus and Trix. I believe I can do that at Basecamp where I'll be part of a team that embraces experimentation.

---

**To my next point - I like solving problems**.

I like to think of problem solving as an activity involving **mindfulness meditation** - being conscious of the problem at hand, while also having situational awareness. I'm also aware that creativity can sometimes get you carried away, so I'm always being careful and can narrow down when I need to. If a situation needs me to dig deeper, I'm not afraid. One important lesson that I learned while working with startups is to *expect the unexpected* - so better be prepared!

The role you describe requires these skills - striking a balance between a creative enterprise versus staying grounded and focused - and I can do a great job at it.  

---

**I can write really really well.**

I write to think, and I write to educate. It's also one of the ways how I learn to build software. I'll let [my work](https://booleanhunter.gitlab.io/basecamp-application/posts/my-portfolio/#writings) speak for itself - you be the judge!

---

**I can write clean code**.

Not the kind of code that'll make me look clever or cooler in a *look-how-I-did-this-in-1-line* sort-of way - I'm more like [your boring programmer](https://m.signalvnoise.com/im-a-boring-programmer-and-proud-of-it/) 🤣. I prioritize writing code that's human-readable over machine-readable. I like writing code that's [well documented](https://github.com/booleanhunter/ReactJS-AdminLTE/tree/master/src/components). I see the value in effective abstractions.

I tend to not re-invent the wheel, except when it comes to personal learning. I'm also a generalist so I like using the right set of tools for the right situations.

---

**To my final point, I believe in shared values.**

Coworkers can be some of the most influential people in our lives, and I want to be amongst the company of people who believe in values like honesty, kindness, helpfulness, and most importantly - **courage**. I know that when the situation calls for it, you're not afraid to challenge the status quo.

Looking at the team page on your website, It's obvious to me that there are a lot of values that we share, even though we come from different cultures.  

---

## Why Basecamp?
 
Sure, the benefits and salaries are great. But its not going to be easy - most great things aren't! I'm very likely competing against the best available programmers & writers in the world for this role, and you want the best. The work will be challenging, and I'm not anxious because of it, I'm excited. After all, anxiety and excitement are often the same thing, it's just the framing.

I've just begun to read **Shape Up** and **Rework** and I'm beginning to understand your work ethics. Someday if I do decide to become an entrepreneur, I would want to build an organization the way you do. Until then, I want to [earn to give](https://en.wikipedia.org/wiki/Earning_to_give), and be an advocate of the same values that Basecamp is built upon.

And last but not the least, it's [your attitude on important and difficult issues](https://m.signalvnoise.com/testimony-before-the-house-antitrust-subcommittee/) when the world looks the other way. Your fearlessness is something that I deeply admire, and I wish to walk that path with you. By being in your company, I know that I will be able to manifest that same courage and wisdom that you express, every day.