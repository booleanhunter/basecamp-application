+++
title = "Some of my works that you'll find interesting"
date = "2020-01-28"
+++

Several of my projects span across activities like learning, coding, and writing - so I've categorized them to especially emphasize the major activity involved while creating them:

---
### Learning & open-source

* My very early experiment with ReactJS when I was still a newbie - [ReactJS-AdminLTE](https://github.com/booleanhunter/ReactJS-AdminLTE). It's now archived - mostly because I struggled to keep it updated - but I wish to re-open it again. I wonder how open-source developers even manage to keep their projects alive, **AND** do their everyday job!?

* I like to ask questions & seek answers. [Here's a question on Webpack](https://stackoverflow.com/questions/28969861/managing-jquery-plugin-dependency-in-webpack) that I asked on stack-overflow, and [a related blog post](https://blog.booleanhunter.com/webpack-for-the-fast-and-the-furious/) that also got published on [freeCodeCamp](https://www.freecodecamp.org/news/webpack-for-the-fast-and-the-furious-bf8d3746adbd/).

* [Some of my experiments on Machine Learning](https://github.com/booleanhunter/ML-supervised-learning)

* I'm a sucker for clean user interfaces, so I created my own [tech blog](https://blog.booleanhunter.com/) and theme, inspired by Medium's reading experience. I'm also in the midst of converting it to a PWA, which is still a [work-in-progress](https://develop.blog.booleanhunter.com/).

* My interests go beyond programming as well. Here's a new, [incomplete pet-project](https://develop.functionallyfortified.fit/), where I plan to share stuff related to nutrition and fitness.

---
### Writings

* [How to build your own Uber-for-X App](https://blog.booleanhunter.com/how-to-build-your-own-uber-for-x-app/): A two-part blog series and my greatest hit. Related [Github repo](https://github.com/booleanhunter/how-to-build-your-own-uber-for-x-app).

* [How to use Data Science to Understand What Makes Wine Taste Good](https://blog.booleanhunter.com/using-data-science-to-understand-what-makes-wine-taste-good/) - my most fun project 🍷

* [What I think programming is](https://www.quora.com/What-are-some-great-truths-of-computer-programming/answer/Ashwin-Hariharan-4).

* [My views on the startup life](https://blog.booleanhunter.com/the-idea-of-an-invigorating-startup-life-can-often-be-an-illusion/)

* Like you Basecamp, I try to use my powers of writing for good and fight evil. [Here's an article](https://www.quora.com/What-is-wrong-with-India-2/answer/Ashwin-Hariharan-4) I wrote to make people aware of Aadhaar and its dangers. Got viewed by several prominent technologists and columnists in India.

* [On women-in-tech](https://code.likeagirl.io/the-cultural-conundrums-of-women-in-tech-that-we-need-to-be-aware-of-be3a3489ab92).

---
### I like to help other people write as well

Was part of the editorial team at **[freeCodeCamp](https://www.freecodecamp.org/news/announcing-our-freecodecamp-2018-top-contributor-award-winners-861da08a77e1/)** & the [managing editor](https://code.likeagirl.io/help-us-support-code-like-a-girl-editors-4f529c94b37b) at **Code Like A Girl** before they moved away from medium. Here are some articles I helped edit and publish during that time:

* [Pragmatic rules of web accessibility that will stick to your mind](https://medium.com/free-code-camp/pragmatic-rules-of-web-accessibility-that-will-stick-to-your-mind-9d3eb85a1a28)
* [A research roundup to show that your office layout is toxic](https://code.likeagirl.io/a-research-roundup-to-show-that-your-office-layout-is-toxic-and-some-tips-for-making-it-better-8434864b0ab2)
* [The Incomplete Deep Learning Guide](https://medium.com/free-code-camp/the-incomplete-deep-learning-guide-2cc510cb23ee)
* [Meditation will make you a better programmer. Here's how](https://medium.com/free-code-camp/meditation-will-make-you-a-better-programmer-heres-how-1c0582f675fa)

I'm also currently part of the editorial team at **[Towards Data Science](https://towardsdatascience.com/our-team-c2c8e712c971)**.

---

### I like to mentor and teach

* Was a co-organizer for freeCodeCamp's local chapter in Bangalore, India from 2017 to 2018.

* Was a classroom mentor & Project Reviewer at Udacity for their Machine Learning Engineer Nanodegree for over 50 students.

* Have helped several people in my circles get programming jobs.
