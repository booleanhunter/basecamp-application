+++
title = "A fairly recent web technology that I like"
date = "2020-01-27"
+++
**Love this question!**

I've come to recognize and appreciate the utility of static site generators. I think they're an amazing piece of technology, leveraging something as old as the web itself (they're just static documents!) to create a beautiful experience on the browser. One particular example that stands out is **Gatsby.js**.

To provide some context:

I'm using Hugo (another static site generator) to generate pages for my [blog](https://blog.booleanhunter.com). The reason I chose it, (and still continue to do so for most of my projects, including this website) is because it's super-fast, and there are a host of themes that you can choose from to get your site running. However - for one of my projects, I find that my needs are slowly outgrowing what Hugo can currently provide:

- **Conditionally rendering a UI widget**: I find myself increasingly relying on Hugo shortcodes and global  page variables, and it gets harder to write logic that way after a while. Whereas, a component-based design that encapsulates its own logic would be much easier to use.
- **Code maintenance**: It's hard to encapsulate a UI widget's look-and-feel with its logic together. With Hugo, I'm being forced to write global event listeners in one place, which kind-of gets convoluted when I add more features.
- **Portability**: It's also getting a bit harder for me to port a certain widget and its logic to another blog or project.
- **Adding more features**: I also find myself writing code more often than I'd like to, for example - adding PWA capabilities.

---
To summarize, as my project continues to grow, I felt like I needed to consider other alternatives - that'll allow me to not only introduce new features easily, but also reduce my effort at maintaining and extending existing ones. Upon exploring several other options, I came across Gatsby.js. Some of its features I find super-cool are:

- It is super-fast, almost on par with Hugo - so rapid page generation is a walk-in-the-park.
- It provides PWA capabilities out-of-the-box.
- It leverages React.js and React's component-driven architecture. That means code maintenance and extensibility are much easier to achieve. I can also very easily port a UI component along with its logic to another project.

Besides, the documentation is robust, and if there's any specific feature not shipped with the main project, I can find a plugin for it or create one myself! That's really convenient. The only downside I find is that you'd have to get a bit more technical to be able to use it. I also wish that there were more themes available in the ecosystem (maybe I'll write one!)

Hugo is more friendly for the non-tech crowd. I've only had a brief exposure to Go (the  language Hugo uses), but I had no problems using it. With Gatsby, I'll need to understand React -  at-least a little bit. But hey, that's not a problem for me! 


Here's a visual - 

{{< image src="./hugoVgatsby.png" alt="Hugo versus Gatsby" position="center"  >}} 

The "effort involved" in the visual indicates the overall effort that it takes to not just build your application, but also stuff like code-maintenance or adding more features. Hugo starts off having lesser features than Hugo, but the effort to learn and use is low. On the other hand, Gatsby starts off with more features, but there's also more effort involved in the beginning.

And as your project keeps growing, it becomes increasingly difficult to use Hugo. At some-point, you'll want to explore other options - and Gatsby would be a very good one!


And later, I'm also keen to explore how to improve performance. One possible area worth looking at is predictive caching of commonly requested resources. Here's an [interesting project](https://github.com/guess-js/guess/tree/master/experiments/guess-static-sites), one that I haven't looked into fully yet.